import { Button, Window, WindowHeader, WindowContent } from 'react95'
import { useState, useEffect } from 'react'

function ensureLength(array, length) {
  while (array.length < length) {
    array = [...array, ...array]
  }
  return array.slice(0, length)
}

export default props => {
  const w = 5
  const h = 5
  const words = ensureLength(
    [
      'Skjut mig',
      'Kokt i skit',
      'Bajja',
      'Gamlingar',
      'Kärnfamilj',
      'Donken',
      'Jag mår dåligt',
      'Valentino',
      'Googla på det',
      'Toabrink',
      'Det blir ingen lunch',
      'Det är i köket',
    ],
    w * h
  )
  const things = words
    .sort(item => 0.5 - Math.random())
    .map(item => {
      return {
        active: false,
        bingo: false,
        text: item,
      }
    })
  const [state, setState] = useState(things)
  const [bingo, setBingo] = useState(false)
  const click = (thing, index) => {
    setState(
      state.map((item, i) => {
        if (i === index) item.active = !item.active
        return item
      })
    )
    // Vertical check
    for (let x = 0; x < w; ++x) {
      let c = 0
      for (let y = 0; y < h; ++y) {
        if (state[y * h + x].active) {
          c++
        }
      }
      if (c === w) {
        console.log('Vertical', c)
        setState(
          state.map((item, i) => {
            if (i % w === x) item.bingo = true
            return item
          })
        )
        setBingo(true)
        return
      }
    }
    // Horizontal check
    for (let y = 0; y < h; ++y) {
      let c = 0
      for (let x = 0; x < w; ++x) {
        if (state[y * h + x].active) {
          c++
        }
      }
      if (c === h) {
        console.log('Horizontal', c)
        setState(
          state.map((item, i) => {
            if (i >= y * h && i < y * h + w) item.bingo = true
            return item
          })
        )
        setBingo(true)
        return
      }
    }
    setBingo(false)
    setState(
      state.map((item, i) => {
        item.bingo = false
        return item
      })
    )
  }
  return (
    <Window>
      <WindowHeader>bingo95.exe</WindowHeader>
      <WindowContent>
        {state.map((thing, index) => (
          <Button
            key={[index, thing.active]}
            onClick={event => click(thing, index)}
            active={thing.active}
            style={{
              width: 100 / w + '%',
              minHeight: '5em',
              background: thing.bingo ? 'tomato' : 'inherit',
            }}
          >
            {thing.text}
            {thing.bingo && 'BINGO'}
          </Button>
        ))}
        <hr />
        <Button
          onClick={event => {
            setState(things)
            setBingo(false)
          }}
        >
          Reset
        </Button>
        <Button disabled>Exit</Button>
      </WindowContent>
    </Window>
  )
}
