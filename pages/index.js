import { AppBar, Toolbar, Button } from 'react95'
import Bingo from '../components/Bingo.js'

export default props => {
  return (
    <div>
      <AppBar>
        <Toolbar style={{ justifyContent: 'space-between' }}>
          <Button disabled>BravoBingo</Button>
          <Button>Reset</Button>
        </Toolbar>
      </AppBar>
      <Bingo />
    </div>
  )
}
