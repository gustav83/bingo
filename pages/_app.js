import { createGlobalStyle, ThemeProvider } from 'styled-components'
import { reset, themes } from 'react95'

const ResetStyles = createGlobalStyle`
  ${reset}
  body, html {
    padding: 5rem;
    background: teal;
  }
`

export default props => {
  const { Component, pageProps, reduxStore } = props
  return (
    <div className="App">
      <ResetStyles />
      <ThemeProvider theme={themes.default}>
        <Component {...pageProps} />
      </ThemeProvider>
    </div>
  )
}
